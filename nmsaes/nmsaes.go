package nmsaes

import (
	"github.com/forgoer/openssl"
)

// NmsAesEncrypt 加密
func NmsAesEncrypt(src, key []byte) ([]byte, error) {
	dst, err := openssl.AesECBEncrypt(src, key, openssl.PKCS7_PADDING)
	//return string(dst)
	return dst, err
}

//NmsAseDecrypt 解密
func NmsAseDecrypt(dst, key []byte) ([]byte, error) {
	dst, err := openssl.AesECBDecrypt(dst, key, openssl.PKCS7_PADDING)
	return dst, err
}
